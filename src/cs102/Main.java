package cs102;

public class Main {

    public static void main(String[] args) {
        DriverLicense driverLicense = new DriverLicense("George", 123456, "2020-12-31", "A+");
        System.out.println(driverLicense);

        driverLicense.increasePenalty(80);
        System.out.println(driverLicense);

        driverLicense.increasePenalty(21);
        System.out.println(driverLicense);

        driverLicense.decreasePenalty(51);
        System.out.println(driverLicense);

        driverLicense.decreasePenalty(31);
        System.out.println(driverLicense);

        driverLicense.decreasePenalty(18);
        System.out.println(driverLicense);
    }
}
