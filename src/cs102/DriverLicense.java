package cs102;

public class DriverLicense {
    private String driverName;
    private int driverID;
    private String expirationDate;
    private String bloodType;
    private int penalty;
    private boolean status;
    private int limit;

    public DriverLicense(String dn, int di, String ed, String bt) {
        driverName = dn;
        driverID = di;
        expirationDate = ed;
        bloodType = bt;
        penalty = 0;
        status = true;
        limit = 100;
    }

    public void increasePenalty(int p) {
        penalty += p;
        adjustStatus();
    }

    public void decreasePenalty(int p) {
        if (p < penalty) {
            penalty -= p;
        } else {
            penalty = 0;
        }
        adjustStatus();
    }

    private void adjustStatus() {
        if (penalty >= limit) {
            status = false; /* License is blocked. */
        } else if (penalty < 20 && status == false) {
            status = true; /* License is active. */
        }
    }

    public String getDriverName() {
        return driverName;
    }

    public int getDriverID() {
        return driverID;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public String getBloodType() {
        return bloodType;
    }

    public int getPenalty() {
        return penalty;
    }

    public boolean isStatus() {
        return status;
    }

    public int getLimit() {
        return limit;
    }

    public void setExpirationDate(String ed) {
        expirationDate = ed;
    }

    public void setLimit(int l) {
        limit = l;
    }

    public String toString() {
        return "Driver License information:\n" +
                "------------------------\n" +
                "Driver Name: " + driverName + "\n" +
                "Driver ID: " + driverID + "\n" +
                "Expiration Date: " + expirationDate + "\n" +
                "Blood Type: " + bloodType + "\n" +
                "Total Penalty: " + penalty + "\n" +
                "Status: " + rephraseStatus() + "\n" +
                "Limit: " + limit + "\n\n";
    }

    private String rephraseStatus() {
        if (status) {
            return "Active";
        }
        return "Blocked";
    }
}
